# Gitlab_385268

## Name

Testing https://gitlab.com/gitlab-org/gitlab/-/issues/385268.

## Description

According to the output of the last job running on the linked repo, the pipeline is green, [reference](https://cdn.artifacts.gitlab-static.net/94/a9/94a98e1f7f987f18d06ffff4b5a0c871d543a6c440a81c28a58afa8d0ed4a88f/2022_12_09/3450337629/3769376981/job.log?response-content-type=text%2Fplain%3B%20charset%3Dutf-8&response-content-disposition=inline&Expires=1671697520&KeyName=gprd-artifacts-cdn&Signature=Px3dZ5XiR3XSdeiG5P5cz8s43jw=), I'll use this repo to test whether the issue is resolved.
